import pprint
import requests 
import datetime
import random
import json
import os


def create(data, files):

    _URL_READ = "https://data.jabarprov.go.id/api/3/action/resource_list"
    _URL_CREATE = "https://data.jabarprov.go.id/api/3/action/resource_create"

    file_path = "/mnt/d/SERVER/PythonProject/JDS/ckan-bulk-upload/csv/kemitraan.csv"
    file_name = os.path.basename(file_path)
    file_size = os.path.getsize(file_path)
    _DATA = { 
        "package_id": "name",
        # "url": "url",
        "revision_id": "title",
        "description": "description",
        "format": "csv",
        # "hash": "hash",
        "name": "name",
        # "resource_type": "resource_type",
        "mimetype": "text/csv",
        # "mimetype_inner": "mimetype_inner",
        # "cache_url": "cache_url",
        "size": file_size,
        "created": datetime.datetime.now(),
        "last_modified": datetime.datetime.now(),
        "cache_last_updated": datetime.datetime.now(), 
    }

    _FILES = {
        "upload": (file_name, open(file_path, "rb"), 'application-type'),
    }

    _HEADERS = {
        "Authorization": "e68fcff1-ecc8-453b-ad4e-46993df7e2ea", 
    }

    print(datetime.datetime.now())
    # res_read = requests.get(url=_URL_READ, headers=_HEADERS) 
    res_create = requests.post(url=_URL_CREATE, data=data, files=files, headers=_HEADERS) 

    # result_read = res_read.json() 
    result_create = res_create.json() 

    # print(result_read)
    print(result_create) 
    return result_create