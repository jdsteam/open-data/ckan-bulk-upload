import pprint
import requests 


def create(data):

    _URL_READ = "https://data.jabarprov.go.id/api/3/action/organization_list"
    _URL_CREATE = "https://data.jabarprov.go.id/api/3/action/organization_create"
    _DATA = {
        "id": "id",
        "name": "name",
        "title": "title",
        "description": "description",
        "image_url": "https://www.google.co.id/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png",
        "type": "organization",
        "state": "active",
        "approval_status": "approved",
        # "extras": [{
        #     "key": "kunci",
        #     "value": "isi"
        # }],
    }

    _HEADERS = {
        "Authorization": "e68fcff1-ecc8-453b-ad4e-46993df7e2ea",
        "Content-Type": "application/json"
    }

    # res_read = requests.get(url=_URL_READ, headers=_HEADERS) 
    res_create = requests.post(url=_URL_CREATE, json=data, headers=_HEADERS) 

    # result_read = res_read.json() 
    result_create = res_create.json() 

    # print(result_read)
    print(result_create) 