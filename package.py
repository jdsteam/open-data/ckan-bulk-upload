import pprint
import requests 


def create(data):

    _URL_READ = "https://data.jabarprov.go.id/api/3/action/package_list"
    _URL_CREATE = "https://data.jabarprov.go.id/api/3/action/package_create"
    _DATA = { 
        "name": "name",
        "title": "title",
        "private": "false",
        "author": "author",
        "author_email": "author_email@email.com",
        "maintainer": "maintainer",
        "maintainer_email": "maintainer_email@email.com",
        "license_id": "gfdl",
        "notes": "notes",
        "url": "url",
        "version": "version",
        "state": "active",
        "type": "dataset",
        # "resources": "resources",
        "tags": [{
            "name": "name"
        }],
        "extras": [{
            "key": "kunci",
            "value": "isi"
        }],
        "groups": [{
            "name": "name"
        }],
        "owner_org": "name"
    }

    _HEADERS = {
        "Authorization": "e68fcff1-ecc8-453b-ad4e-46993df7e2ea",
        "Content-Type": "application/json"
    }

    # res_read = requests.get(url=_URL_READ, headers=_HEADERS) 
    res_create = requests.post(url=_URL_CREATE, json=data, headers=_HEADERS) 

    # result_read = res_read.json() 
    result_create = res_create.json() 

    # print(result_read)
    print(result_create) 