import group
import organization
import package
import resource  
import datetime
import random
import json
import os
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import logging

if __name__ == '__main__':

    logging.basicConfig(level=logging.DEBUG,
        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        datefmt='%m-%d %H:%M',
        filename='log/resources.log',
        filemode='a+') 
    console = logging.StreamHandler()
    console.setLevel(logging.INFO) 
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s') 
    console.setFormatter(formatter) 
    logging.getLogger('').addHandler(console)

    logging.info('This is log \n') 
    logger1 = logging.getLogger('wilayah_merge')

    data = {}
    # group.create(data)
    # organization.create(data)
    # package.create(data)
    # resource.create(data)
 

    # group
    # df = pd.read_excel('dataset_opendata.xlsx', sheet_name='group')  
    # nama_group = df['nama_group'].values.tolist() 
    # foto = df['foto'].values.tolist()  
    
    # for index in range(0, 10): 
    #     print(index, nama_group[index], foto[index]) 
    #     data = { 
    #         "name": nama_group[index].lower().replace(" &", "").replace(" ", "-"),
    #         "title": nama_group[index],
    #         "description": "",
    #         "image_url": foto[index],
    #         "type": "group",
    #         "state": "active",
    #         "approval_status": "approved", 
    #     }
    #     print(data)
    #     group.create(data)


    # organization
    # df = pd.read_excel('dataset_opendata.xlsx', sheet_name='organization')  
    # nama_dinas = df['nama_dinas'].values.tolist() 
    # foto = df['foto'].values.tolist()  
    
    # for index in range(0, 38): 
    #     print(index, nama_dinas[index], foto[index]) 
    #     data = { 
    #         "name": nama_dinas[index].lower().replace(",", "").replace(" ", "-"),
    #         "title": nama_dinas[index],
    #         "description": "",
    #         "image_url": foto[index],
    #         "type": "organization",
    #         "state": "active",
    #         "approval_status": "approved", 
    #     }
    #     print(data)
    #     organization.create(data)



    # package
    # df = pd.read_excel('dataset_opendata.xlsx', sheet_name='package')  
    # nama_dinas = df['nama_dinas'].values.tolist() 
    # judul_dataset = df['judul_dataset'].values.tolist()  
    
    # for index in range(0, 433): 
    #     print(index, nama_dinas[index], judul_dataset[index]) 
    #     data = { 
    #         "name": judul_dataset[index].lower().replace(" ", "-").replace("/", "").replace("(", "").replace(")", "").replace(",", "-"),
    #         "title": judul_dataset[index],
    #         "private": "false",
    #         "author": nama_dinas[index], 
    #         "maintainer": nama_dinas[index], 
    #         "license_id": "gfdl", 
    #         "version": "1.0",
    #         "state": "active",
    #         "type": "dataset",  
    #         "owner_org": nama_dinas[index].lower().replace(",", "").replace(" ", "-")
    #     }
    #     print(data)
    #     package.create(data)
        



    # resource
    df = pd.read_excel('dataset_opendata.xlsx', sheet_name='KATALOG_DATASET')  
    nama_dinas = df['nama_dinas'].values.tolist() 
    judul_dataset = df['judul_dataset'].values.tolist()  
    nama_file_data = df['nama_file_data'].values.tolist()   

    for index in range(0, 1738): 
        # print(index, nama_dinas[index], judul_dataset[index])     

        try:
            file_path = "/mnt/d/SERVER/PythonProject/JDS/ckan-bulk-upload/csv/"+nama_file_data[index]+".csv"
            f = open(file_path)
            f.close()
            file_name = os.path.basename(file_path)
            file_size = os.path.getsize(file_path)
            date_upload = datetime.datetime.now()
            exist = True
            data = {  
                "package_id": judul_dataset[index].lower().replace(" ", "-").replace("/", "").replace("(", "").replace(")", "").replace(",", "-"),
                "format": "csv", 
                "name": nama_file_data[index].replace("-", " ").title(), 
                "mimetype": "text/csv", 
                "size": file_size,
                "created": date_upload,
                "last_modified": date_upload,
                "cache_last_updated": date_upload, 
            }

            files = {
                "upload": (file_name, open(file_path, "rb"), 'application-type'),
            } 

            logger1.info(str(index+1) + " " + str(exist) + " " +  nama_file_data[index])
            res = resource.create(data, files)
            logger1.info(res)
            logging.info('\n') 

        except Exception as e: 
            exist = False
            logger1.info(str(index+1) + " " + str(exist) + " " +  nama_file_data[index])
            print(e)
            logger1.info(e)
            logging.info('\n') 
        
         